package wdMethods1;

import java.io.IOException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {
	public static ExtentHtmlReporter html;
	public static ExtentTest test;
	public static ExtentReports extent;
	public static String testCaseId,testDescription,author,category,desc,status;
	@BeforeSuite (groups = "common")
	public void startResult()
	{
		html=new ExtentHtmlReporter("./report/results.html");
		html.setAppendExisting(true);
		extent= new ExtentReports();
		extent.attachReporter(html);
		
	}
	@BeforeMethod (groups = "common")
	public void startTest()
	{
		test=extent.createTest(testCaseId, testDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
		
	}
	
	public void reportStep(String desc,String status)
	{
		if(status.equalsIgnoreCase("Pass"))
		{	
			test.pass(desc);
		}
		else
		{
            test.fail(desc);
		}
       
	}
	
	@AfterSuite
	public void complete()
	{
	 extent.flush();
	}}
