package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods1.ProjectMethods;

public class TC005_EditLead extends ProjectMethods{
	
	/*@BeforeClass (groups = "common")
	public void setdata()
	{
		tcname ="TC005_EditLead";
		tcdesc = " edit lead ";
		author = " prasanth ";
		catogory = "smoke";
	}*/
	
	@Test (/*dependsOnMethods = "testcases.TC002_CreateLead.create" */groups = "sanity" , dependsOnGroups = "smoke")
	public void EditLead() throws InterruptedException {
		
		
		
		//login();
		WebElement link2 = locateElement("linktext","Lea");
		click(link2);
		
		WebElement link3 = locateElement("linktext","Find Leads");
		click(link3);
		
		WebElement link4 = locateElement("xpath","//div[@class='x-form-item x-tab-item']/div[@class='x-form-element']/input[@name='firstName']");
		type(link4,"prasanth");
		
		WebElement link5 = locateElement("xpath","//button[@class='x-btn-text'][text()='Find Leads']");
		click(link5);
		
		Thread.sleep(3000);
		
		WebElement link6 = locateElement("xpath","(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		click(link6);
		
		 verifyTitle("Edit Lead");
		
					
		WebElement link7 = locateElement("id","viewLead_companyName_sp");
		getText(link7);
		
		
		WebElement link8 = locateElement("xpath","//a[text()='Edit']");
		click(link8);
		
		WebElement link9 = locateElement("id","updateLeadForm_companyName");
		link9.clear();
		type(link9,"SoftTechie Systems");
		
		WebElement link10 = locateElement("xpath","//input[@class='smallSubmit'][@value='Update']");
		click(link10);
		
		WebElement link11 = locateElement("id","viewLead_companyName_sp");
		getText(link11);
	
		
		
		
		
		
		
	}

}
