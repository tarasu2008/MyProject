package testcases;

import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] getExcelData(String filename) throws IOException {
		// TODO Auto-generated method stub
		
		//locatefolder
		XSSFWorkbook wbook = new XSSFWorkbook("./data/createlead.xlsx");

		 XSSFSheet sheet = wbook.getSheet("createlead");
		 
		 int lastRowNum = sheet.getLastRowNum();
		 System.out.println(lastRowNum);
		 
		 int columnCount = sheet.getRow(0).getLastCellNum();
		 Object[][] data = new Object[lastRowNum][columnCount];
		 System.out.println(columnCount);
		 
		 
		 for (int j = 1; j < lastRowNum; j++) {
			 
			XSSFRow row = sheet.getRow(j);
			
			for (int i = 0; i <columnCount ; i++) {
				XSSFCell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				data[j-1][i] = stringCellValue;
				System.out.println(stringCellValue);
			} 
		}
		return data;
		 
	}

}
