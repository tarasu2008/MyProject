package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdMethods1.ProjectMethods;



public class TC002_CreateLead extends ProjectMethods{
	@BeforeClass (groups = "common")
	public void setdata()
	{
		tcname ="TC002_CreateLead";
		tcdesc = " create lead ";
		author = " prasanth ";
		catogory = "smoke";
	}
	//@Test (/*invocationCount = 2 , invocationTimeOut = 30000,*/ groups = "smoke")
	
	@Test(groups = "smoke",
			dataProvider = "newtesting")
	
	
	/*public Object[][] create()
	{
		//login1();
		
		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);
		WebElement element1 = locateElement("id", "createLeadForm_companyName");
		type(element1, "tcs");
		WebElement element2 = locateElement("id", "createLeadForm_firstName");
		type(element2, "prasanth");
		WebElement element3 = locateElement("id", "createLeadForm_lastName");
		type(element3, "ravi");
		WebElement dd1 = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(dd1, "Direct Mail");
		WebElement dd2 = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(dd2, "Automobile");
		WebElement e1 = locateElement("xpath", "//input[@class='smallSubmit']");
		click(e1);
	}*/
		
		public void createLead(String cname, String fname,
				String lname,String email, String ph) {
		
		
		    click(locateElement("linkText", "CRM/SFA"));
			click(locateElement("linkText", "Create Lead"));
			type(locateElement("id", "createLeadForm_companyName"), cname);
			type(locateElement("id", "createLeadForm_firstName"), fname);
			type(locateElement("id", "createLeadForm_lastName"), lname);
			type(locateElement("id", "createLeadForm_primaryEmail"), email);
			type(locateElement("id", "createLeadForm_primaryPhoneNumber"), ph);
			click(locateElement("name", "submitButton"));			
		}
		
		@DataProvider(name = "newtesting")
		public Object[][] getData() {
			Object[][] data =
					new Object[2][5];
			data[0][0] = "testleaf";
			data[0][1] = "koushik";
			data[0][2] = "c";
			data[0][3] = "ko@a.com";
			data[0][4] = "123";
			
			data[1][0] = "wipro";
			data[1][1] = "sethu";
			data[1][2] = "c";
			data[1][3] = "ko@a.com";
			data[1][4] = "123";
			
			return data;
		
		
		
			
		
		
		
		}






		





	}


