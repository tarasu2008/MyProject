package wdMethods1;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import testcases.ReadExcel;

public class ProjectMethods extends SeMethods {
	
	@DataProvider (name= "fetchdata")
	public Object[][] getData() throws IOException {
		Object[][] excelData = ReadExcel.getExcelData("./data/createlead.xlsx");
			return excelData;
	}
	
	//@BeforeMethod (groups = "common")
	@Parameters({"browser",  "appUrl", "userName", "password" })
	public void login(String browserName, String appUrl,String userName, String password ) {
		startApp(browserName, appUrl);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, userName );
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("Class","decorativeSubmit");
		click(eleLogin);
		
	}
	//@AfterMethod (groups = "common")
	public void closingBrowser()
	{
		closeAllBrowsers();
	}
}
